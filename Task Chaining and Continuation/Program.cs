﻿using System;
using System.Threading.Tasks;

namespace Task_Chaining_and_Continuation
{
    /// <summary>
    /// Console application class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// Application entrance.
        /// </summary>
        /// <param name="args">Argumnets given to program.</param>
        /// <returns>Task to run async.</returns>
        public static async Task Main(string[] args)
        {
            var random = new Random();

            var task = Task<int[]>.Run(() => ArrayOperations.CreateTenRandomInts());

            await task
                .ContinueWith(t => ArrayOperations.MultiplyOnRandom(t.Result, random.Next(199)))
                .ContinueWith(t => ArrayOperations.SortAsc(t.Result))
                .ContinueWith(t => ArrayOperations.Average(t.Result));
        }
    }
}
